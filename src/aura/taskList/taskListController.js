({
	addItem : function(cmp, event, helper) {
		helper.addItem(cmp);
	},
    
    doInit : function (cmp, event, helper) {
        var action = cmp.get('c.getTasks');
        
        action.setParams({
            whoId: cmp.get('v.recordId')  
        });
        
        action.setCallback(this, function (result){
           var taskList = result.getReturnValue();
           cmp.set('v.taskList', taskList);
            
        });
        
        $A.enqueueAction (action);
    }
})