({
	addItem : function(cmp) {
		var task = cmp.get('v.task');
        task.WhoId = cmp.get('v.recordId');
        var taskList = cmp.get('v.taskList');
        
        
        var action = cmp.get('c.createTask');
        
        action.setParams({
            t: task
        });
        
        action.setCallback(this, function(result){
            var state = result.getState();
            
            if (state === 'SUCCESS') {
                var createdTask = result.getReturnValue();
                console.log('createdTask', createdTask);
                taskList.push(createdTask);
                cmp.set('v.taskList', taskList);
        		cmp.set('v.task', {});
                
            } else { //the rest. most likely error state
                console.log('addItem callback error ', result.getError()[0]);
            }
        });
        
        $A.enqueueAction (action);
        
	}
})