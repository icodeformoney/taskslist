public class TaskListCtrl {
    
    @AuraEnabled
    public static Task createTask (Task t) {
        try {
            insert t;
            return t; 
        } catch (Exception ex) {
            System.debug (ex.getMessage());
            throw new AuraHandledException (ex.getMessage() + '-' + ex.getStackTraceString());
        }
        
    }
    
    @AuraEnabled
    public static List <Task> getTasks (String whoId) {
        return [SELECT Id, Subject, WhoId FROM Task WHERE WhoId =:whoId];
    }
}